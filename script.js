const gameContainer = document.getElementById("game");
const startGame = document.getElementById("start")
const score = document.getElementById("score")
const winnerMessage = document.getElementById("message")
const time = document.getElementById("time");
const resetButton = document.getElementById("resetButton")
const exit = document.getElementById("exitButton")
const topScore = document.getElementById("highest-Score")


let cards = [];
let count = 0;
let moves = 0
let seconds = 60;
let arrayLength

let backgroundImage = "image/dark-chain-background.png"
// const COLORS = [
//   "red",
//   "blue",
//   "green",
//   "orange",
//   "purple",
//   "red",
//   "blue",
//   "green",
//   "orange",
//   "purple"
// ];

const IMAGES = [
  "./gifs/1.gif",
  "./gifs/2.gif",
  "./gifs/3.gif",
  "./gifs/4.gif",
  "./gifs/5.gif",
  "./gifs/6.gif",
  "./gifs/7.gif",
  "./gifs/8.gif",
  "./gifs/9.gif",
  "./gifs/10.gif",
  "./gifs/11.gif",
  "./gifs/12.gif",
  "./gifs/1.gif",
  "./gifs/2.gif",
  "./gifs/3.gif",
  "./gifs/4.gif",
  "./gifs/5.gif",
  "./gifs/6.gif",
  "./gifs/7.gif",
  "./gifs/8.gif",
  "./gifs/9.gif",
  "./gifs/10.gif",
  "./gifs/11.gif",
  "./gifs/12.gif"  
];
// Start the game 

function start(){

  startGame.style.visibility = "hidden";
  
  createDivsForColors(shuffledColors);
  
  setInterval(countdown, 1000);
}
// initialize local storage

if (typeof(Storage) !== "undefined") {

  if (localStorage.getItem("highestScore") === null) {
      localStorage.setItem("highestScore", count);
  }

  var value = localStorage.getItem("highestScore");
  topScore.innerText = "Highest Score: "+value;

} else {
  topScore.innerText = "Highest Score: 0";
}

// countdown timer 
function countdown(){
  seconds--;
  time.innerText = 'Timer : ' + seconds +' seconds';
  localStorage.setItem("highestScore", count);
  if(seconds === 0){
    gameContainer.textContent="Better luck next time !";
    gameContainer.style.fontSize="2em"
    setTimeout(()=>{exitGame()},5000)
    
  }
}
exit.addEventListener("click", exitGame)
resetButton.addEventListener("click", resetGame);

// to exit the game 
function exitGame() {
  location.reload();
}

// reset the game
function resetGame(){
removeCards()  
clearTimeout()

cards = [];
count = 0;
moves = 0
seconds = 60;
start()

}

//remove deck cards
function removeCards(){
  while (gameContainer.hasChildNodes()) {
    gameContainer.removeChild(gameContainer.firstChild);
  }
}



// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by gifs/1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }
  arrayLength = array.length;
  return array;
}

let shuffledColors = shuffle(IMAGES);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
  for (let color of colorArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(color);
    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);
    
    // append the div to the element with an id of game
    gameContainer.append(newDiv);
   console.log(newDiv,"newDiv")
  }
}




// TODO: Implement this function!
function handleCardClick(event) {
  // you can use event.target to see which element was clicked
  console.log("you clicked",event.target);
  cards.push(event);
  if(cards.length<=2){
    let value = event.target.classList.value;
    event.target.style.backgroundImage =`url(${value})`;
    event.target.style.backgroundRepeat ="no-repeat";
    event.target.style.backgroundSize ="auto";
    cards[0].target.removeEventListener("click",handleCardClick, false);

  }
  if(cards.length==2){
     compareTwoCards();
  }

}



// when the DOM loads


function compareTwoCards(){

 if(cards[0].target.classList.value == cards[1].target.classList.value){
  console.log("Match found!");
  cards[0].target.removeEventListener("click",handleCardClick, false);
  cards[1].target.removeEventListener("click",handleCardClick, false);
  //cards.splice(0, cards.length);  
  cards=[]
  count++;
  score.innerHTML = count
  //console.log("count", count)
  if(count==arrayLength/2){

   //alert("You Win!! You have found the all the correct matches")
   gameContainer.textContent="You Win!! You have found the all the correct matches";
   gameContainer.style.fontSize="2em"
   setTimeout(()=>{exitGame()},5000)
  }
} else {
  setTimeout(()=>{
    cards[0].target.addEventListener("click",handleCardClick);
    cards[0].target.style.backgroundImage=`url(${backgroundImage})`;
    cards[1].target.style.backgroundImage=`url(${backgroundImage})`;
  cards=[]
  },1000)   
}

}
